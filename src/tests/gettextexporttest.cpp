/*
* This file is part of Lokalize
*
* SPDX-FileCopyrightText: 2023 Azamat H. Hackimov <azamat.hackimov@gmail.com>
*
* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#include <QTest>
#include "gettextexport.h"

namespace GettextCatalog
{

class GettextExportTest : public QObject
{
    Q_OBJECT
public:
    std::unique_ptr<GettextExportPlugin> plugin;
private Q_SLOTS:
    void testWriteComment_data();
    void testWriteComment();

    void testWriteKeyword_data();
    void testWriteKeyword();
};

void GettextExportTest::testWriteComment_data()
{
    QTest::addColumn<QString>("comment");
    QTest::addColumn<QString>("formattedComment");

    QTest::addRow("One line comment") << "Some comment"
                                    << "# Some comment\n";
    QTest::addRow("Multiline comment") << "Some comment\nAnd another line"
                                     << "# Some comment\n# And another line\n";
}

void GettextExportTest::testWriteComment()
{
    QFETCH(QString, comment);
    QFETCH(QString, formattedComment);

    plugin = std::make_unique<GettextExportPlugin>(80, 1);
    QString output;
    QTextStream stream(&output);
    plugin->writeComment(stream, comment);
    QCOMPARE(output, formattedComment);
}

void GettextExportTest::testWriteKeyword_data()
{
    QTest::addColumn<int>("wrap");
    QTest::addColumn<QString>("keyword");
    QTest::addColumn<QString>("text");
    QTest::addColumn<QString>("expected");

    // Some simple and basic messages without sophisticated formatting
    QTest::addRow("80-msgid-empty") << 80 << "msgid" << "" << "msgid \"\"\n";
    QTest::addRow("80-msgid-short") << 80 << "msgid"
                                    << "Short message"
                                    << "msgid \"Short message\"\n";
    QTest::addRow("80-msgstr-short-multiline") << 80 << "msgstr" <<
        "Multi\\n"
        "line" <<
        "msgstr \"\"\n"
        "\"Multi\\n\"\n"
        "\"line\"\n";

    QTest::addRow("80-msgid-tokenizer-in-action") << 80 << "msgid" <<
        "./krita/data/bundles/Krita_4_Default_Resources.bundle:brushes/chisel_knife.png" <<
        "msgid \"\"\n"
        "\"./krita/data/bundles/Krita_4_Default_Resources.bundle:brushes/\"\n"
        "\"chisel_knife.png\"\n";

    QTest::addRow("60-msgid-long-with-mixed-newlines") << 60 << "msgid" <<
        "Test message with old 60 lines wrap and \n"
        "unintended and\\n\n"
        "intended newlines." <<
        "msgid \"\"\n"
        "\"Test message with old 60 lines wrap and unintended and\\n\"\n"
        "\"intended newlines.\"\n";
}

void GettextExportTest::testWriteKeyword()
{
    QFETCH(int, wrap);
    QFETCH(QString, keyword);
    QFETCH(QString, text);
    QFETCH(QString, expected);

    plugin = std::make_unique<GettextExportPlugin>(wrap, 1);
    QString output;
    QTextStream stream(&output);
    plugin->writeKeyword(stream, keyword, text);
    QCOMPARE(output, expected);
}

}

QTEST_GUILESS_MAIN(GettextCatalog::GettextExportTest)

#include "gettextexporttest.moc"
